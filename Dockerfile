FROM vminnovations/dotnet-sdk:3.1

COPY helloworld.sh /
RUN chmod 700 /helloworld.sh

ENTRYPOINT /helloworld.sh
